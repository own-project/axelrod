import numpy as np


class Cell:

    def __init__(self, numStates):
        self.state = np.zeros(numStates, dtype=numpy.int8)

    def getState(self):
        return self.state

    def setstate(self, state):
        self.state = state
